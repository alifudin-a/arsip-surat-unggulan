package models

type StatusSuratMasuk struct {
	ID     int    `json:"id" db:"id"`
	Status string `json:"status" db:"status"`
}

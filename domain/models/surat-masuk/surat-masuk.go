package models

import (
	"github.com/alifudin-a/arsip-surat-puskom/domain/helper"
)

type SuratMasuk struct {
	ID         int64             `json:"id" db:"id"`
	Tanggal    string            `json:"tanggal" db:"tanggal"`
	Nomor      string            `json:"nomor" db:"nomor" validate:"required"`
	IDPengirim int64             `json:"id_pengirim" db:"id_pengirim" validate:"required"`
	Perihal    string            `json:"perihal" db:"perihal" validate:"required"`
	IDJenis    *int64            `json:"id_jenis" db:"id_jenis"`
	Keterangan *string           `json:"keterangan" db:"keterangan"`
	IDStatus   helper.NullInt64  `json:"id_status,omitempty" db:"id_status"`
	Upload     helper.NullString `json:"upload,omitempty" db:"upload"`
	Filename   helper.NullString `json:"filename,omitempty" db:"filename"`
	CreatedAt  helper.NullString `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt  helper.NullString `json:"updated_at,omitempty" db:"updated_at"`
}

type Penerima struct {
	IDs        int64             `json:"-" db:"id"`
	IDSurat    int64             `json:"-" db:"id_surat"`
	IDPengguna int64             `json:"id_penerima" db:"id_pengguna"`
	CreatedAt2 helper.NullString `json:"-,omitempty" db:"created_at"`
	UpdatedAt2 helper.NullString `json:"-,omitempty" db:"updated_at"`
}

type ListSuratMasuk struct {
	ID         int64             `json:"id" db:"id"`
	Tanggal    string            `json:"tanggal,omitempty" db:"tanggal" validate:"required"`
	Nomor      string            `json:"nomor,omitempty" db:"nomor" validate:"required"`
	IDPengirim int64             `json:"id_pengirim,omitempty" db:"id_pengirim" validate:"required"`
	Pengirim   string            `json:"pengirim,omitempty" db:"pengirim"`
	Perihal    string            `json:"perihal,omitempty" db:"perihal" validate:"required"`
	IDJenis    *int64            `json:"id_jenis,omitempty" db:"id_jenis"`
	Jenis      *string           `json:"jenis,omitempty" db:"jenis"`
	Keterangan *string           `json:"keterangan,omitempty" db:"keterangan"`
	CreatedAt  helper.NullString `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt  helper.NullString `json:"updated_at,omitempty" db:"updated_at"`
	Upload     helper.NullString `json:"upload,omitempty" db:"upload"`
	Filename   helper.NullString `json:"filename,omitempty" db:"filename"`
	IDStatus   helper.NullInt64  `json:"id_status,omitempty" db:"id_status"`
	Status     helper.NullString `json:"status,omitempty" db:"status"`
	Base64     helper.NullString `json:"base64,omitempty"`
}

type CreateSuratMasuk struct {
	SuratMasuk
	Penerima
}

package builder

import (
	"strings"

	models "github.com/alifudin-a/arsip-surat-puskom/domain/models/login"
	repository "github.com/alifudin-a/arsip-surat-puskom/repository/login"
)

func Login(params *models.Login) repository.LoginParams {
	var resp repository.LoginParams

	lowercaseUsername := strings.ToLower(params.Username)

	resp.Username = lowercaseUsername

	return resp
}

package query

var ListSuratMasukDesc = `
select 
	ts.id, 
	ts.tanggal, 
	ts.nomor, 
	ts.id_pengirim, 
	tp."name" as pengirim,
	ts.id_jenis,
	tjs."name" as jenis,
	ts.perihal, 
	ts.keterangan,
	ts.upload,
	ts.filename,
	ts.id_status,
	ts2.status
from 
	tbl_surat ts
left join tbl_pengguna tp on tp.id = ts.id_pengirim
left join tbl_jenis_surat tjs on tjs.id = ts.id_jenis
left join tbl_status ts2 on ts2.id = ts.id_status
order by id desc;`

var ListSuratMasukAsc = `
select 
	ts.id, 
	ts.tanggal, 
	ts.nomor, 
	ts.id_pengirim, 
	tp."name" as pengirim,
	ts.id_jenis,
	tjs."name" as jenis,
	ts.perihal, 
	ts.keterangan,
	ts.upload,
	ts.filename,
	ts.id_status,
	ts2.status
from 
	tbl_surat ts
left join tbl_pengguna tp on tp.id = ts.id_pengirim
left join tbl_jenis_surat tjs on tjs.id = ts.id_jenis
left join tbl_status ts2 on ts2.id = ts.id_status
order by id asc offset $1;`

var GetSuratMasukByID = `
select 
	ts.id, 
	ts.tanggal, 
	ts.nomor, 
	ts.id_pengirim, 
	tp."name" as pengirim,
	ts.id_jenis,
	tjs."name" as jenis,
	ts.perihal, 
	ts.keterangan,
	ts.upload,
	ts.filename,
	ts.id_status,
	ts2.status
from 
	tbl_surat ts
left join tbl_pengguna tp on tp.id = ts.id_pengirim
left join tbl_jenis_surat tjs on tjs.id = ts.id_jenis
left join tbl_status ts2 on ts2.id = ts.id_status
where ts.id = $1;`

var DeletePenerimaSurat = `DELETE FROM tbl_penerima WHERE id_surat = $1;`

var DeleteSuratMasuk = `DELETE FROM tbl_surat WHERE id = $1;`

var IsPenerimaSuratExist = `SELECT COUNT(*) FROM tbl_penerima WHERE id_surat = $1;`

var IsSuratMasukExist = `SELECT COUNT(*) FROM tbl_surat WHERE id= $1;`

var ReadSuratMasukByIDPenerimaAndID = `
select 
	ts.id, 
	ts.tanggal, 
	ts.nomor, 
	ts.id_pengirim, 
	tp."name" as pengirim,
	ts.id_jenis,
	tjs."name" as jenis,
	ts.perihal, 
	ts.keterangan,
	ts.upload,
	ts.filename,
	ts.id_status,
	ts2.status
from 
	tbl_surat ts
left join tbl_pengguna tp on tp.id = ts.id_pengirim
left join tbl_jenis_surat tjs on tjs.id = ts.id_jenis
left join tbl_penerima tp2 on tp2.id_surat = ts.id
left join tbl_status ts2 on ts2.id = ts.id_status
where tp2.id_pengguna = $1 and ts.id = $2;`

var ListSuratMasukByIDPenerima = `
select 
	ts.id, 
	ts.tanggal, 
	ts.nomor, 
	ts.id_pengirim, 
	tp."name" as pengirim,
	ts.id_jenis,
	tjs."name" as jenis,
	ts.perihal, 
	ts.keterangan,
	ts.upload,
	ts.filename,
	ts.id_status,
	ts2.status
from 
	tbl_surat ts
left join tbl_pengguna tp on tp.id = ts.id_pengirim
left join tbl_jenis_surat tjs on tjs.id = ts.id_jenis
left join tbl_penerima tp2 on tp2.id_surat = ts.id
left join tbl_status ts2 on ts2.id = ts.id_status
where tp2.id_pengguna = $1 order by id DESC;`

var ListSuratMasukByIDPenerimaAsc = `
select 
	ts.id, 
	ts.tanggal, 
	ts.nomor, 
	ts.id_pengirim, 
	tp."name" as pengirim,
	ts.id_jenis,
	tjs."name" as jenis,
	ts.perihal, 
	ts.keterangan,
	ts.upload,
	ts.filename,
	ts.id_status,
	ts2.status
from 
	tbl_surat ts
left join tbl_pengguna tp on tp.id = ts.id_pengirim
left join tbl_jenis_surat tjs on tjs.id = ts.id_jenis
left join tbl_penerima tp2 on tp2.id_surat = ts.id
left join tbl_status ts2 on ts2.id = ts.id_status
where tp2.id_pengguna = $1 order by id ASC OFFSET $2;`

var CreateSuratMasuk = `
	insert 
		into 
			tbl_surat (
				tanggal, 
				nomor, 
				id_pengirim, 
				perihal, 
				id_jenis, 
				keterangan, 
				created_at,
				upload,
				filename,
				id_status
			) values(
				$1,$2,$3,$4,$5,$6,$7,$8,$9,$10
			) RETURNING *;`

var CreatePenerimaSuratMasuk = `
	insert 
		into 
			tbl_penerima (
				id_surat, 
				id_pengguna, 
				created_at
			) values ($1, $2, $3) returning *;
`

var UpdateSuratMasuk = `
UPDATE 
	tbl_surat
SET 
	tanggal = $1, 
	nomor = $2, 
	id_pengirim = $3, 
	perihal = $4,
	id_jenis = $5,
	keterangan = $6,
	updated_at = $7,
	upload = $8,
	filename = $9,
	id_status = $10
WHERE
	id = $11
RETURNING *;`

var UpdatePenerimaSuratMasuk = `
insert 
into 
	tbl_penerima (
		id_surat, 
		id_pengguna, 
		created_at,
		updated_at
	) values ($1, $2, $3, $4) returning *;
`
var CekNomorSuratMasuk = `
	SELECT COUNT(*) FROM tbl_surat WHERE nomor = $1;
`

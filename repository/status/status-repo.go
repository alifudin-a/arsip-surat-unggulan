package repository

import (
	database "github.com/alifudin-a/arsip-surat-puskom/database/psql"
	models "github.com/alifudin-a/arsip-surat-puskom/domain/models/status"
	"github.com/alifudin-a/arsip-surat-puskom/domain/query"
)

type repo struct{}

type StatusRepository interface {
	GetAllStatus() ([]models.StatusSuratMasuk, error)
}

func NewStatusRepository() StatusRepository {
	return &repo{}
}

func (*repo) GetAllStatus() ([]models.StatusSuratMasuk, error) {
	var status []models.StatusSuratMasuk
	var db = database.DB

	err := db.Select(&status, query.GetAllStatus)
	if err != nil {
		return nil, err
	}

	return status, nil
}

package main

import (
	database "github.com/alifudin-a/arsip-surat-puskom/database/psql"
	rd "github.com/alifudin-a/arsip-surat-puskom/database/redis"
	"github.com/alifudin-a/arsip-surat-puskom/domain/helper"
	"github.com/alifudin-a/arsip-surat-puskom/route"
)

func main() {
	helper.GetDriveService()
	rd.OpenRedis()
	database.OpenDB()
	route.InitRoute()
}

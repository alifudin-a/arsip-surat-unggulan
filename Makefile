gorun:
	go run main.go

tidy:
	go mod tidy

#gobuild:
#CGO_ENABLED=0 go build -o bin/arsip-surat-unggulan
#exec:
#./bin/arsip-surat-unggulan

gobuild:
	go build -o arsip-surat-unggulan

exec:
	./arsip-surat-unggulan

startapp: gobuild exec

develop:
	git push origin develop

gpo:
	git push origin

gpom:
	git push origin master

gphm:
	git push heroku master

push: gpom gphm

##server##
appname := arsip-surat-unggulan

redeploy-app: build restart log

run:
	go run main.go

build:
	go build -o $(appname) main.go && chmod +x $(appname)

update:
	supervisorctl update

restart:
	supervisorctl restart $(appname)

start:
	supervisorctl start $(appname)

stop:
	supervisorctl stop $(appname)

createlog:
	@touch /var/log/$(appname).log

log: 
	tail -f /var/log/$(appname).log

##docker-compose##
dcup:
	docker-compose up -d

dcdown:
	docker-compose down

dcstart:
	docker-compose start

dcstop:
	docker-compose stop

dcappup:
	docker-compose up -d app

dcappstart:
	docker-compose start app

dcappstop:
	docker-compose stop app

dclogs:
	docker-compose logs -f app

dcrestart: dcstop dcstart

redeploy: dcappstop dbuild dprune dcappup dclogs



#Dockerfile#
dbuild:
	docker build . -t arsip-surat-unggulan

dprune:
	docker image prune -f
	
logs:
	docker logs arsip-surat-unggulan -f

dstop:
	docker container stop arsip-surat-unggulan

ctl: dstop ip

#Run Docker#
dredeploy: dcdown dbuild dprune dcup

#Psql#
bash:
	docker exec -it pq-arsip-surat-unggulan bash

dump:
	docker exec -it pq-arsip-surat-unggulan pg_dump -U postgres db_arsip_surat > sql_dump/db_arsip_surat_22062021.sql

import:
	docker exec -i pq-arsip-surat-unggulan psql -U postgres db_arsip_surat < sql_dump/db_arsip_surat_16062021.sql

genip = `docker exec -i pq-arsip-surat-unggulan hostname -i | awk '{print $1}'`
envdir = `~/Dev/puskom/arsip-surat-puskom/.env`


ip: 
	echo "DB_HOST="$(genip)

stop-container:
	docker container stop arsip-surat-unggulan

conf: ip stop-container

#redis#
rd-cli:
	docker exec -it rd-arsip-surat-unggulan redis-cli

#remove file
rmsk:
	rm static/uploads/surat_keluar/*

rmsm:
	rm static/uploads/surat_masuk/*

festart:
	cd ~/Dev/react.js/aplikasi-surat-unggulan; npm start;

feopen:
	cd ~/Dev/react.js/aplikasi-surat-unggulan; code .;
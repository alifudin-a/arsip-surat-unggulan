package rd

import (
	"log"

	"context"

	"github.com/go-redis/redis/v8"
)

var RC *redis.Client
var ctx = context.Background()

func OpenRedis() {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       3,
	})

	pong, err := client.Ping(ctx).Result()
	if err != nil {
		log.Println("An error occured while sending PING to redis : ", err)
	}

	log.Println("Redis connection =====> PING :", pong)

	RC = client
}

package action

import (
	"context"
	"log"
	"net/http"
	"strconv"

	rdc "github.com/alifudin-a/arsip-surat-puskom/database/redis"
	"github.com/alifudin-a/arsip-surat-puskom/domain/helper"
	models "github.com/alifudin-a/arsip-surat-puskom/domain/models/surat-keluar"
	repository "github.com/alifudin-a/arsip-surat-puskom/repository/surat-keluar"
	"github.com/alifudin-a/arsip-surat-puskom/route/middleware"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
)

type Read struct{}

func NewReadSuratKeluar() *Read {
	return &Read{}
}

func (rd *Read) ReadSuratKeluarHandler(c echo.Context) (err error) {
	var resp helper.Response
	var suratKeluar *models.ReadSuratKeluar

	err = middleware.ValidationJWT(c)
	if err != nil {
		return
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resp.Code = http.StatusBadRequest
		resp.Message = "ID harus berupa angka!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	repo := repository.NewSuratKeluarRepository()

	arg := repository.ReadSuratKeluarParams{
		ID: int64(id),
	}

	exist, err := repo.IsSuratKeluarExist(repository.IsSuratKeluarExistParams{ID: int64(id)})
	if err != nil {
		return
	}

	if !exist {
		resp.Code = http.StatusBadRequest
		resp.Message = "Data Surat tidak ada!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	suratKeluar, err = repo.FindByID(arg)
	if err != nil {
		return err
	}

	resp.Code = http.StatusOK
	resp.Message = "Berhasil Menampilkan Surat Keluar!"
	resp.Body = map[string]interface{}{
		"surat": suratKeluar,
	}

	return c.JSON(http.StatusOK, resp)
}

func (rd *Read) ReadSuratKeluarByIDPenggunaAndID(c echo.Context) (err error) {

	var resp helper.Response
	var suratKeluar *models.ReadSuratKeluar

	err = middleware.ValidationJWT(c)
	if err != nil {
		return
	}

	id1, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resp.Code = http.StatusBadRequest
		resp.Message = "ID harus berupa angka!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	id2, err := strconv.Atoi(c.Param("id_s"))
	if err != nil {
		resp.Code = http.StatusBadRequest
		resp.Message = "ID harus berupa angka!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	repo := repository.NewSuratKeluarRepository()

	arg := repository.FindByIDandIDPenggunaParams{
		IDPengguna: int64(id1),
		ID:         int64(id2),
	}

	suratKeluar, err = repo.FindByIDandIDPengguna(arg)
	if err != nil {
		return err
	}

	resp.Code = http.StatusOK
	resp.Message = "Berhasil Menampilkan Surat Keluar!"
	resp.Body = map[string]interface{}{
		"surat": suratKeluar,
	}

	return c.JSON(http.StatusOK, resp)
}

func (rd *Read) GetBase64Handler(c echo.Context) (err error) {
	var resp helper.Response
	var rc = rdc.RC
	var ctx = context.Background()

	err = middleware.ValidationJWT(c)
	if err != nil {
		return err
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resp.Code = http.StatusBadRequest
		resp.Message = "ID harus berupa angka!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	res, err := rc.Get(ctx, strconv.Itoa(id)).Result()
	if err == redis.Nil {
		log.Println("[Base64-Handler] ", err)
	} else if err != nil {
		log.Println("[Base64-Handler] ", err)
	}

	resp.Code = http.StatusOK
	resp.Message = "Berhasil Menampilkan Base64!"
	resp.Body = map[string]interface{}{
		"base64": res,
	}

	return c.JSON(http.StatusOK, resp)
}

package action

import (
	"net/http"

	"github.com/alifudin-a/arsip-surat-puskom/domain/helper"
	models "github.com/alifudin-a/arsip-surat-puskom/domain/models/status"
	repository "github.com/alifudin-a/arsip-surat-puskom/repository/status"
	"github.com/alifudin-a/arsip-surat-puskom/route/middleware"
	"github.com/labstack/echo/v4"
)

type s struct{}

func NewGetAllStatusHandler() *s {
	return &s{}
}

func (*s) GetAllStatusHandler(c echo.Context) (err error) {
	var resp helper.Response
	var status []models.StatusSuratMasuk

	err = middleware.ValidationJWT(c)
	if err != nil {
		return err
	}

	repo := repository.NewStatusRepository()

	status, err = repo.GetAllStatus()
	if err != nil {
		return err
	}

	resp.Code = http.StatusOK
	resp.Message = "Berhasil mendapatkan status!"
	resp.Body = map[string]interface{}{
		"status": status,
	}

	return c.JSON(http.StatusOK, resp)
}

package action

import (
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/alifudin-a/arsip-surat-puskom/domain/builder"
	"github.com/alifudin-a/arsip-surat-puskom/domain/helper"
	models "github.com/alifudin-a/arsip-surat-puskom/domain/models/login"
	repository "github.com/alifudin-a/arsip-surat-puskom/repository/login"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

type Login struct{}

func NewLoginHandler() *Login {
	return &Login{}
}

type LoginTokenClaim struct {
	*jwt.StandardClaims
	models.Login
}

func (lg *Login) validate(req *models.Login, c echo.Context) (err error) {
	if err = c.Bind(req); err != nil {
		return
	}

	return c.Validate(req)
}

func (lg *Login) ExLoginHandler(c echo.Context) (err error) {

	var resp helper.Response
	var login *models.Login

	// err = mid.ValidationKey(c)
	// if err != nil {
	// 	return
	// }

	if err = c.Bind(&login); err != nil {
		return
	}

	username := login.Username
	password := login.Password

	lowercaseUsername := strings.ToLower(username)

	repo := repository.NewLoginRepository()

	arg := repository.ExLoginParams{
		Username: lowercaseUsername,
		Password: password,
	}

	login, err = repo.ExLogin(arg)
	if err != nil {
		resp.Code = http.StatusUnauthorized
		resp.Message = "Login gagal! Periksa kembali username dan password anda!"
		return c.JSON(http.StatusUnauthorized, resp)
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = &LoginTokenClaim{
		&jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		},
		models.Login{
			ID:         login.ID,
			IDPengguna: login.IDPengguna,
			Username:   login.Username,
			CreatedAt:  login.CreatedAt,
			UpdatedAt:  login.UpdatedAt,
		},
	}

	tokenJwt, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return err
	}

	resp.Code = http.StatusOK
	resp.Message = "Login Berhasil!"
	resp.Body = map[string]interface{}{
		"token": tokenJwt,
	}

	return c.JSON(http.StatusOK, resp)
}

func (lg *Login) LoginHandler(c echo.Context) (err error) {

	var resp helper.Response
	var req = new(models.Login)
	var login *models.Login

	err = lg.validate(req, c)
	if err != nil {
		resp.Code = http.StatusInternalServerError
		resp.Message = "Gagal memvalidasi data!"
		return c.JSON(http.StatusInternalServerError, resp)
	}

	// username := login.Username

	// lowercaseUsername := strings.ToLower(username)

	// req.Username = lowercaseUsername

	repo := repository.NewLoginRepository()

	arg := builder.Login(req)

	login, err = repo.Login(arg)
	if err != nil {
		resp.Code = http.StatusUnauthorized
		resp.Message = "Login gagal! Periksa kembali Username dan Password Anda!"
		return c.JSON(http.StatusUnauthorized, resp)
	}

	err = bcrypt.CompareHashAndPassword([]byte(login.Password), []byte(req.Password))
	if err != nil {
		resp.Code = http.StatusUnauthorized
		resp.Message = "Login gagal! Periksa kembali Username dan Password Anda!"
		return c.JSON(http.StatusUnauthorized, resp)
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = &LoginTokenClaim{
		&jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		},
		models.Login{
			ID:         login.ID,
			IDPengguna: login.IDPengguna,
			Username:   login.Username,
			CreatedAt:  login.CreatedAt,
			UpdatedAt:  login.UpdatedAt,
		},
	}

	tokenJwt, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return err
	}

	resp.Code = http.StatusOK
	resp.Message = "Login Berhasil!"
	resp.Body = map[string]interface{}{
		"token": tokenJwt,
	}

	return c.JSON(http.StatusOK, resp)
}

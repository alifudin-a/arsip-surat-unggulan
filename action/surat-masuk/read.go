package action

import (
	"context"
	"log"
	"net/http"
	"strconv"

	rdc "github.com/alifudin-a/arsip-surat-puskom/database/redis"
	"github.com/alifudin-a/arsip-surat-puskom/domain/helper"
	models "github.com/alifudin-a/arsip-surat-puskom/domain/models/surat-masuk"
	repository "github.com/alifudin-a/arsip-surat-puskom/repository/surat-masuk"
	"github.com/alifudin-a/arsip-surat-puskom/route/middleware"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
)

type Read struct{}

func NewReadSuratMasuk() *Read {
	return &Read{}
}

func (rd *Read) ReadSuratMasukHandler(c echo.Context) (err error) {
	var resp helper.Response
	var suratMasuk *models.ListSuratMasuk

	err = middleware.ValidationJWT(c)
	if err != nil {
		return
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resp.Code = http.StatusBadRequest
		resp.Message = "ID harus berupa angka!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	repo := repository.NewSuratMasukRepository()

	arg := repository.GetSuratMasukParams{
		ID: int64(id),
	}

	exist, err := repo.IsSuratMasukExist(repository.IsSuratMasukExistParams{ID: int64(id)})
	if err != nil {
		return
	}

	if !exist {
		resp.Code = http.StatusBadRequest
		resp.Message = "Data Surat tidak ada!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	suratMasuk, err = repo.FindByID(arg)
	if err != nil {
		return
	}

	resp.Code = http.StatusOK
	resp.Message = "Berhasil menampilkan surat masuk!"
	resp.Body = map[string]interface{}{
		"surat_masuk": suratMasuk,
	}

	return c.JSON(http.StatusOK, resp)
}

func (rd *Read) GetBase64Handler(c echo.Context) (err error) {
	var resp helper.Response
	var rc = rdc.RC
	var ctx = context.Background()

	err = middleware.ValidationJWT(c)
	if err != nil {
		return err
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resp.Code = http.StatusBadRequest
		resp.Message = "ID harus berupa angka!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	res, err := rc.Get(ctx, strconv.Itoa(id)).Result()
	if err == redis.Nil {
		log.Println("[Base64-Handler] ", err)
	} else if err != nil {
		log.Println("[Base64-Handler] ", err)
	}

	resp.Code = http.StatusOK
	resp.Message = "Berhasil Menampilkan Base64!"
	resp.Body = map[string]interface{}{
		"base64": res,
	}

	return c.JSON(http.StatusOK, resp)
}
